package products.laptop;

import java.util.Scanner;

public class LaptopUtil {
	
	static int total;
	
	static {
		total=0;
	}
	
	Laptop l1;
	public LaptopUtil()
	{
		l1 = new Laptop();
			
	}
	
		public void selectBrand() {
			
			System.out.println("Select Brand\n");
			System.out.println("1.Dell \t2.HP");
			int choice;
			
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
			
			
			switch(choice) {
			
			case 1 : l1.setBrand("Dell");
							System.out.println("Bucket : "+total+" Rs.");
			
			break;
			
			case 2 : l1.setBrand("HP");
							System.out.println("Bucket : "+total+" Rs.");
			break;
			}
			
			}
		
			public void selectRam() {
				
				System.out.println("Select RAM\n");
				System.out.println("1. 4 GB - 10000 Rs \t2.8 GB - 20000");
				int choice;
				
				Scanner sc = new Scanner(System.in);
				choice = sc.nextInt();
				
				
				switch(choice) {
				
				case 1 : l1.setRam(4);
								total=total+10000;
								System.out.println("Bucket : "+total+" Rs.");
				
				break;
				
				case 2 : l1.setRam(8);
								total=total+20000;
								System.out.println("Bucket : "+total+" Rs.");
				
				break;
				}
				
				}	
		
			
			public void selectHdd() {
				
				System.out.println("Select Hard Disk Drive\n");
				System.out.println("1. 500 GB - 20000 Rs. \t2. 1000 GB - 40000");
				int choice;
				
				Scanner sc = new Scanner(System.in);
				choice = sc.nextInt();
				
				
				switch(choice) {
				
				case 1 : l1.setHdd(500);
								total=total+20000;
								System.out.println("Bucket : "+total+" Rs.");
				
				break;
				
				case 2 : l1.setHdd(1000);
								total=total+40000;
								System.out.println("Bucket : "+total+ " Rs.");
				break;
				
				}
			}
				
				
				
			public void disp() {
					
					System.out.println("Selected Laptop :"+l1.getBrand()+"\nRAM : "+l1.getRam()+" GB"+"\nHDD : "+l1.getHdd()+" GB");
				}	
		
		
	
		public void calculateTotatl() {
		
		System.out.println("Checkout price :"+total+" Rs.");
	}

}
